<!DOCTYPE html>
<html>
<head>
	<title>Form SignUp</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>

	<h3>Sign Up Form</h3>
	<form action="/welcome" method="POST">
		@csrf
		<label>First name:</label><br><br>
			<input type="text" name="firstname"><br><br>
		<label>Last name:</label><br><br>
			<input type="text" name="lastname"><br><br>
		<label>Gender:</label><br><br>
		 	<input type="radio" name="gender_male">
			<label>Male</label><br>
			<input type="radio" name="gender_female">
			<label>Female</label><br>
			<input type="radio" name="gender_other">
			<label>Other</label>
			<br><br>
		<label>Nationality:</label><br><br>
			<select name="nationality">
				<option value="indonesian">Indonesia</option>
				<option value="singapore">Singapore</option>
				<option value="malaysia">Malaysia</option>
				<option value="autralia">Australia</option>
			</select>
			<br><br>
		<label>Language Spoken:</label><br><br>
			<input type="checkbox" name="spoken_indonesia">
			<label>Bahasa Indonesia</label><br>
			<input type="checkbox" name="spoken_english">
			<label>English</label><br>
			<input type="checkbox" name="spoken_other">
			<label>Other</label>
			<br><br>
		<label>Bio:</label><br><br>
			<textarea name="bio" rows="5"></textarea>
			<br>
		<input type="submit" value="Sign Up">
	</form>
</body>
</html>